#ifndef MAIN_H
# define MAIN_H

int		AGGRCOW(void);
int		BALLSUM(void);
int 	ABCDEF(void);
int 	PAYING(void);
int		LASTDIG(void);
int		AKVQLD03(void);
int 	MLASERP(void);
int     FCTRL(void);
int 	DQUERY(void);
int 	TRVCOST(void);

#endif
