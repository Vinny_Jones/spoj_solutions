#include <iostream>

long long int	find_gcd(long long int nbr1,long long int nbr2);

int		BALLSUM(void)
{
	long long int nbr1, nbr2, p, q, gcd;

	while(1) 
	{
		std::cin >> nbr1 >> nbr2;
		if (nbr1 == -1 && nbr2 == -1)
			break;
		q = (nbr1 * (nbr1 - 1)) / 2;

		if (nbr2 % 2)
			p = (nbr2 / 2) * (nbr2 / 2);
		else
			p = (nbr2 - 2) / 2 * (nbr2 / 2);
		gcd = find_gcd(p, q);
		p /= gcd;
		q /= gcd;
		if (p <= 0)
			std::cout << 0 << std::endl;
		else if (q == 1)
			std::cout << 1 << std::endl;
		else
			std::cout << p << '/' << q << std::endl;
	}
    return (0);
}

long long int	find_gcd(long long int nbr1,long long int nbr2)
{
	if (nbr2 == 0)
		return (nbr1);
	else
		return (find_gcd(nbr2, nbr1 % nbr2));
}