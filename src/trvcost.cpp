#include <iostream>
#include <vector>
#include <climits>
#include <queue>
#define UI unsigned int
using namespace std;

typedef struct	s_edge {
	void 	*ptr;
	UI		dist;
}				t_edge;

typedef struct	s_vtx{
	UI				cost;		// = UINT_MAX;
	bool 			visited;	// = false;
	vector<t_edge>	edges;
}				vtx;

struct vtx_cmp {
	bool	operator() (vtx *a, vtx *b)
	{
		return (a->cost > b->cost);
	}
};

void 	add_edge(vtx &a, vtx &b, UI cost);
void 	find_path(vector<vtx> &graph, UI start);

int 	TRVCOST(void)
{
	vector<vtx>	graph(501);
	UI	n, a, b, w, i, start;

	for (i = 0; i < 501; ++i) {
		graph[i].cost = UINT_MAX;
		graph[i].visited = false;
	}
	scanf("%u", &n);
	for (i = 0; i < n; ++i) {
		scanf("%u %u %u", &a, &b, &w);
		add_edge(graph[a], graph[b], w);
	}

	scanf("%u", &start);
	find_path(graph, start);
	scanf("%u", &n);
	for (i = 0; i < n; ++i) {
		scanf("%u", &a);
		if (a < graph.size() && graph[a].cost < UINT_MAX)
			cout << graph[a].cost << endl;
		else
			cout << "NO PATH" << endl;
	}
	return (0);
}

void 	find_path(vector<vtx> &graph, UI start)
{
	priority_queue<vtx *, vector<vtx *>, vtx_cmp>	queue;
	vtx			*current, *child;
	UI			child_len;

	graph[start].cost = 0;
	queue.push(&graph[start]);
	while (!queue.empty())
	{
		current = queue.top();
		queue.pop();
		if (current->visited)
			continue ;
		current->visited = true;
		for (int i = 0; i < current->edges.size(); ++i) {
			child = (vtx *)current->edges[i].ptr;
			child_len = current->edges[i].dist;
			if (child->visited)
				continue ;
			if (current->cost + child_len < child->cost)
				child->cost = current->cost + child_len;
			queue.push(child);
		}
	}

}

void 	add_edge(vtx &a, vtx &b, UI dist)
{
	t_edge	temp;

	temp.ptr = &b;
	temp.dist = dist;
	a.edges.push_back(temp);
	temp.ptr = &a;
	b.edges.push_back(temp);
}
