#include <iostream>
#include <algorithm>

#define LIMIT 900030001

int 	find_all(int nbr, long long int *array, int size);

int 	ABCDEF(void)
{
	long long int	*right, *left;
	int 			amount, *numbers, size, result, i, j, k, a, b, c;

	std::cin >> amount;
	size = amount * amount * amount;
	right = new long long int[size];
	left = new long long int[size];
	numbers = new int[amount];
	for (i = 0; i < amount; ++i)
		std::cin >> numbers[i];
	for (i = 0; i < amount; ++i) {
		a = numbers[i];
		for (j = 0; j < amount; ++j) {
			b = numbers[j];
			for (k = 0; k < amount; ++k) {
				c = numbers[k];
				*left++ = a * b + c;
				*right++ = (a) ? a * (b + c) : LIMIT;
			}
		}
	}
	left -= size;
	right -= size;
	std::sort(right, right + size);
	std::sort(left, left + size);

	result = 0;
	for (i = 0; i < size; ++i) {
		result += (find_all(left[i], left, size) * find_all(left[i], right, size));
		while (i < size - 1 && left[i] == left[i + 1])
			i++;
	}

	std::cout << result;

	delete [] numbers;
	delete [] right;
	delete [] left;
	return (0);
}

int 	find_all(int nbr, long long int *array, int size)
{
	int 	start, end, mid, count;

	start = 0;
	end = size - 1;
	while (end >= start)
	{
		mid = (start + end) / 2;
		if (nbr > array[mid])
			start = mid + 1;
		else if (nbr < array[mid])
			end = mid - 1;
		else
			break;
	}
	if (end == start && array[start] != nbr)
		return (0);
	while (mid >= 0 && array[mid] == nbr)
		mid--;
	count = 0;
	while (++mid < size && array[mid] == nbr)
		count++;
	return (count);
}
