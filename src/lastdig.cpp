#include <iostream>

static int 	last_dig(int a, int b);

int		LASTDIG(void)
{
	int 	test_cases, a, b;

	std::cin >> test_cases;
	while (test_cases-- > 0)
	{
		std::cin >> a >> b;
		std::cout << last_dig(a, b) << std::endl;
	}
	return (0);
}

static int 	last_dig(int a, int b)
{
	int mod, res;

	a %= 10;
	if (!a)
		return (0);
	else if (!b || a == 1)
		return (1);
	else if (a == 5 || a == 6)
		return (a);
	mod = (b % 4) ? b % 4 : 4;
	res = a;
	while (--mod > 0)
		res *= a;
	return (res % 10);
}