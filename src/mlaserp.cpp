#include <iostream>
#include <vector>
#include <queue>

using namespace std;

int 	bfs(vector<int>	start, vector<vector <char> > field);

int 	MLASERP(void)
{
	unsigned int	w, h;
	vector<int>		start(2);

	scanf("%d %d", &w, &h);
	vector<vector<char> > field(h + 1, vector<char> (w + 1));
	for (int i = 1; i < h + 1; ++i) {
		scanf("\n");
		for (int j = 1; j < w + 1; ++j) {
			scanf("%c", &field[i][j]);
			if (field[i][j] == 'C' && !start[0])
			{
				field[i][j] = 'S';
				start[0] = i;
				start[1] = j;
			}
		}
	}

	cout << bfs(start, field) << endl;

	return (0);
}

int 	bfs(vector<int>	start, vector<vector <char> > field)
{
	int					result = -1;
	unsigned long int	width = field[0].size();
	unsigned long int	heigth = field.size();
	unsigned long int	next;
	int 				x, y;
	queue<vector<int> >	q;
	vector<int>			temp;

	q.push(start);
	while (!q.empty())
	{
		next = q.size();
		while (next--)
		{
			temp = q.front();
			q.pop();
			x = temp[0];
			y = temp[1];
			if (field[x][y] == 'C')
				return (result);
			if (field[x][y] == '*' || field[x][y] == '*')
				continue ;
			field[x][y] = '*';

			for (int i = 1; x + i < heigth && field[x + i][y] != '*'; ++i) {
				temp[0] = x + i;
				temp[1] = y;
				q.push(temp);
			}
			for (int i = 1; x - i > 0 && field[x - i][y] != '*'; ++i) {
				temp[0] = x - i;
				temp[1] = y;
				q.push(temp);
			}
			for (int i = 1; y + i < width && field[x][y + i] != '*'; ++i) {
				temp[0] = x;
				temp[1] = y + i;
				q.push(temp);
			}
			for (int i = 1; y - i > 0 && field[x][y - i] != '*'; ++i) {
				temp[0] = x;
				temp[1] = y - i;
				q.push(temp);
			}
		}
		result++;
	}
	return (0);
}