#include <iostream>

int     FCTRL(void)
{
	unsigned int			n, result;
	unsigned long long int	nbr, mod;

	scanf("%u", &n);
	while (n--)
	{
		scanf("\n%llu", &nbr);
		for (result = 0, mod = 5; nbr / mod; mod *= 5)
		{
			result += nbr / mod;
		}
		printf("%u\n", result);
	}
    return (0);
}
