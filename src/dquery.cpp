//-----------------
#include <iostream>
//-----------------
#include <cstdio>
#include <algorithm>
using namespace std;

#define Q_MAX 200000

typedef struct	i_elem
{
	unsigned int	type;
	unsigned int	a;
	unsigned int	b;
}				elem;

unsigned int	tree[30001];

void 			update_tree(unsigned int *tree, unsigned int size, unsigned int index, int mod);
int 			input_cmp(const elem &first, const elem &second);
unsigned int	count_unique(unsigned int *tree, int start, int end);

int 	DQUERY(void)
{
	unsigned int	n, q, i;
	unsigned int	*result;
	unsigned int	unique[1000001];

	scanf("%i", &n);
	elem			input[Q_MAX + n + 1];

	for (i = 0; i < 1000001; ++i) {
		unique[i] = 0;
	}
	for (i = 1; i < n + 1; ++i) {
		input[i].type = 0;
		scanf("%i", &input[i].a);
		input[i].b = i;
	}

	scanf("%i", &q);
	for (; i < q + n + 1; ++i) {
		input[i].type = i - n;
		scanf("%i%i", &input[i].a, &input[i].b);
	}
	sort(input + 1, input + n + q + 1, input_cmp);

	for (i = 0; i < n + 1; ++i) {
		tree[i] = 0;
	}
	result = new unsigned int[q];

	for (i = 1; i < n + q + 1; i++) {
		if (input[i].type)
			result[input[i].type - 1] = count_unique(tree, input[i].a, input[i].b);
		else if (!unique[input[i].a])
		{
			unique[input[i].a] = input[i].b;
			update_tree(tree, n, input[i].b, 1);
		}
		else
		{
			update_tree(tree, n, unique[input[i].a], -1);
			update_tree(tree, n, input[i].b, 1);
			unique[input[i].a] = input[i].b;
		}
	}

	for (i = 0; i < q; ++i) {
		printf("%u\n", result[i]);
	}
	return (0);
}

unsigned int	count_unique(unsigned int *tree, int start, int end)
{
	unsigned int result;

	result = 0;
	--start;
	while (end)
	{
		result += tree[end];
		end -= (end & -end);
	}
	while (start)
	{
		result -= tree[start];
		start -= (start & -start);
	}
	return (result);
}

void 			update_tree(unsigned int *tree, unsigned int size, unsigned int index, int mod)
{
	while (index <= size)
	{
		tree[index] += mod;
		index += (index & -index);
	}
}

int 			input_cmp(const elem &first, const elem &second)
{
	if (first.b == second.b)
		return (first.type < second.type);
	return (first.b < second.b);
}
