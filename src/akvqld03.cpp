#include <iostream>
#include <vector>

#define ULL unsigned long long int

void	query_add(std::vector<ULL> &tree, int index, int value);
ULL		query_find(std::vector<ULL> &tree, int start, int end);

int		AKVQLD03(void)
{
	unsigned int		elems, queries, a, b;
	char	c;

	scanf("%d %d", &elems, &queries);
	std::vector<ULL> tree (elems + 1, 0);
	while (queries-- > 0)
	{
		scanf("\n%c%*s %d %d", &c, &a, &b);
		if (c == 'a')
			query_add(tree, a, b);
		else if (c == 'f')
			printf("%llu\n", query_find(tree, a, b));
	}
	return (0);
}

void	query_add(std::vector<ULL> &tree, int index, int value)
{
	ULL	size = tree.size();

	while (index <= size)
	{
		tree[index] += value;
		index += (index & -index);
	}
}

ULL		query_find(std::vector<ULL> &tree, int start, int end)
{
	int size = tree.size() - 1;
	ULL result = 0;

	end = (end > size) ? size : end;
	--start;
	while (end)
	{
		result += tree[end];
		end -= (end & -end);
	}
	while (start)
	{
		result -= tree[start];
		start -= (start & -start);
	}
	return (result);
}
