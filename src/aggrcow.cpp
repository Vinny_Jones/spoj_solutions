#include <iostream>
#include <algorithm>
#include <assert.h>

int 	get_distance(int *arr, int places, int elems);
int		try_place(int *arr, int distance, int places, int elems);

int		AGGRCOW(void)
{
	int 	test_cases, places, elems;
	int 	*arr;

	std::cin >> test_cases;
	while (test_cases-- > 0)
	{
		std::cin >> places >> elems;
		assert(places >= elems);
		arr = new int[places];
		for (int i = 0; i < places; i++)
			std::cin >> arr[i];
		std::sort(arr, arr + places);
		std::cout << get_distance(arr, places, elems) << std::endl;
		delete [] arr;
	}

	return (0);
}

int 	get_distance(int *arr, int places, int elems)
{
	int start, end, mid;

	start = 0;
	end = arr[places - 1] - arr[0] + 1;
	while(end - start > 1)
	{
		mid = (start + end) / 2;
		if (try_place(arr, mid, places, elems))
			start = mid;
		else
			end = mid;

	}
	return (start);
}

int		try_place(int *arr, int distance, int places, int elems)
{
	int 	filled, pos, i;

	filled = 1;
	pos = arr[0];
	for (i = 1; i < places; i++)
		if (arr[i] - pos >= distance)
		{
			++filled;
			if (filled == elems)
				return (1);
			pos = arr[i];
		}
	return (0);
}